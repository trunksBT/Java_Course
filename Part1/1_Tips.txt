0. Additional features for VirtualBox:
    a) Devices -> Insert Guest Additions CD image 

1. Text editor:
    a) The easiest way is "gedit"
    b) The most flexible is "vim"

2. Terminal manager:
    a) The most powerful is "terminator"

3. Each of them you can download with sudo apt-get install "package-name".
    ( without quotation marks ).

4. If you wanna to look on some code with Unit Tests: https://github.com/ProjectsInJava/2015-Pwr-Java-Lib

